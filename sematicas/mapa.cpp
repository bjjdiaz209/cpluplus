#include <string>
#include <iostream>
#include <memory>
#include <map>
#include <functional>
#include <unordered_map>
using namespace std;
using up = unique_ptr<int>;
using nfunc = function<void(int)>;
template<typename T, typename Q, typename R>
void ins(T& m, Q q, R r)
{
  m.insert(make_pair(make_unique<Q>(q),r));
}
void f(int n) {
  std::cout << n*n << '\n';
}
struct up_hash
{
  size_t operator()(const unique_ptr<int>& n) const
  {
    return *n;
  }
};
struct up_eq
{
  bool operator()(const up& a, const up& b)const
  {
    return *a==*b;
  }
};

int main(int argc, char const *argv[])
{

  unordered_map<up, nfunc, up_hash, up_eq> ms;
    ins(ms,64,[](int p){std::cout << p+1 << '\n';});
    ins(ms,125,f);
    ms[make_unique<int>(64)](12);
    int m=200;
    ins(ms,148,[&m](int n){std::cout << m+n << '\n';});
    ms[make_unique<int>(148)](22);
  return 0;
}

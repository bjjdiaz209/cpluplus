#include <iostream>
#include <string>
#include <memory>

using namespace std;

struct mama_de_marco;


struct marco
{
    shared_ptr<mama_de_marco>mdm;
    ~marco()
    {
        cout<<"no te vayas mama\n";
    }
    void hi()
    {
        cout<<"hola mama\n";
    }
};

struct mama_de_marco
{
    weak_ptr<marco> hijo;

    ~mama_de_marco()
    {
        cout<<"adios marco\n";
    }
};




int main(int argc, char const *argv[])
{
    
    auto m= make_shared<marco>();

    auto mdm = make_shared<mama_de_marco>();

    m->mdm= mdm;

    mdm->hijo=m;

    m->hi();

    mdm->hijo.lock()->hi();
}
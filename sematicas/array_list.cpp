#include <iostream>
#include <string>

using namespace std;



class array_list
{
public:
    
    array_list();
    ~array_list();


private:

    struct array_list_iterator
    {
        T* item_pos;

        bool operator!=(const al_it& src)const
        {
            return item_pos != src.item_pos;
        }

        al_it& operator++()
        {
            item_pos++;

            return*this;
        }

        al_it operator++(int)
        {
            al_it n {item_pos};
            item_pos++;
            return n;
        }

        T& operator*()
        {
            return *item_pos;
        }

        const T& operator*()const
        {
            return *item_pos;
        }
    };


public:
    using iterator = al_it;

    iterator begin()
    {
        return iterator{items};

    }

    iterator end()
    {
        return iterator {items+count};
    }

    
};


int main(int argc, char const *argv[])
{
array_list<string> p;

p.push_back("hola");
p.push_back("mundo");
p.push_back("today");
p.push_back("is");
//s.push_back("agosto");s
p.empleace_back("fiday");



for (auto i = p.begin(); i != p.end(); i++)
{
    cout<<*i<<"\n";
}
}
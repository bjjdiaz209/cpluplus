#include <iostream>
#include <string>

using namespace std;
struct A
{
    A(){cout<<"cti\n";}

    ~A(){cout<<"bye";}

    A(const A& x)
    {
        cout<<"copy\n";
    
    }


    A& operator=(const A& x)
    {
        cout<<"=\n";
         return *this;
     }
    

    A(A&&) {cout<<"MOVE\n";}

    A& operator=(A&&)
    {
        cout<<"MOVE =\n";

        return *this;
    }
};

struct B
{
    A a;
    /*B(const A& x): a{x}
     {
        cout<<"charB\n";
     }

  B(A&& x):a{move(x)}
  {
    cout<<"movectirB\n";
  }
  */
 template <class T>
 B(T&& x)
 :a{forward<T>(x)}

 {
    cout<<"ctorB\n";
 }

    
};

int main(int argc, char const *argv[])
{
    
    A a;

    B b{a};

    B c{A{}};

    cout<<":P\n";

}
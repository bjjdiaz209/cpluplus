#include <string>
#include <iostream>
#include <map>
#include <unordered_map>
using namespace std;
ostream& operator<<(ostream& os, const tuple<string, string, int>& x)
{
  os<<get<0>(x)<<" "<<get<1>(x)<<" "<<get<2>(x)<<"";
  return os;
}
int main(int argc, char const *argv[]) {
  std::map<int, tuple<string,string,int>> ps;
  ps.insert(pair<int, tuple<string,string,int>>{101032,tuple<string,string,int>{"juan","perez",11233}});
  ps.insert(make_pair(123412 ,make_tuple("hola","como",4123)));
  ps[462736]=make_tuple("pamela","alvarez",1985);
  for (auto& p:ps) {
    cout << "clave: "<<p.first << " "<< "Valor: "<<p.second << '\n';
  }

  auto& q = ps[1234567];
  std::cout << get<2>(q) << '\n';
  auto r=ps.find(5789273);
  if (r==ps.end()) {
    std::cout << "Not Found" << '\n';
  }
  else
  {
    std::cout << r->second << '\n';
  }

  unordered_map<int, tuple<string, string,int>> qs;
  for (auto& p:ps) {
    qs.insert(p);
  }
  puts("**************");
  for (auto& q:qs) {
    cout << "clave: "<<q.first << " "<< "Valor: "<<q.second << '\n';
  }
  return 0;
}


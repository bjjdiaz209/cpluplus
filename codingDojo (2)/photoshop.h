#ifndef PHOTOSHOP_H
#define PHOTOSHOP_H
#include <memory>
#include <vector>
#include <string>
#include "shape.h"

using namespace std;
template <class RENDERER>
class photoshop
{
private:
    RENDERER m_renderer;
    vector<unique_ptr <shape<RENDERER>>> shapes;
public:
    using iterator =typename vector<unique_ptr <shape<RENDERER>>>::iterator;
    template <typename TYPESHAPE>
    void add_shape(TYPESHAPE&& new_shape)
    {
        shapes.push_back(forward<TYPESHAPE>(new_shape));
    }
    // Move_shape
    // void Resize_shape
    void delete_shape(int p_index)
    {
        shapes.erase(begin()+p_index);
    }

    iterator begin()
    {
        return shapes.begin();
    }

    iterator end()
    {
        return shapes.end();
    }

    shape<RENDERER>& operator[](int index)
    {
        return *shapes[index];
    }
    void show_image() 
    {
        m_renderer.clear();
        for (auto& s: shapes)
        {
            s->paint(m_renderer);
        }
        m_renderer.show();
    }
    // quit
};
#endif
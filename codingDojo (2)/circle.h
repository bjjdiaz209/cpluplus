#ifndef CIRCLE_H
#define CIRCLE_H
#include "shape.h"
template <typename RENDERER>
class circle: public shape<RENDERER>
{
public:
    circle()
    :shape<RENDERER>{"circle"}
    {}
    ~circle()
    {}
    void  paint(RENDERER& p_paint) const override
    {
        
    }

    
};

#endif